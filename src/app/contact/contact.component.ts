import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  submitted = false;
  numberRegex = /^[0-9]+$/;
  civilityChoice = ["Madame", "Monsieur"];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({ // contactForm = FormGroup
      civilite: ['', Validators.required], // civilite = FormControl
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [
        Validators.required, 
        Validators.email
      ]],
      tel: ['', [
        Validators.required, 
        Validators.pattern(this.numberRegex),
      ]],
      sujet: ['', Validators.required],
      message: ['', Validators.required],
      contactAgree: ['', Validators.requiredTrue] // Checkbox must be checked
    });
  }

  get f () { return this.contactForm.controls; }

  onSubmit() {
    // Change variable submitted when user click on button submit "Envoyer ma demande"
    this.submitted = true;

    // Stop here if form is invalid with a empty return
    if (this.contactForm.invalid) {
      console.log(this.contactForm.controls);
      return;
    }

    // If no errors, alert with values in JSON
    alert('Données du formulaire :\n\n' + JSON.stringify(this.contactForm.value, null, 4));
}

}
